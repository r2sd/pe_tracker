const rpn = require("request-promise-native");
const moment = require("moment-timezone");
const nodemailer = require("nodemailer");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

require("dotenv").config();

const schema = new Schema({
  date: {
    type: Date,
    index: {
      unique: true,
      dropDups: true
    }
  },
  pe: Number
});

const formatMomentObject = momentObject =>
  momentObject.tz("Asia/Kolkata").format("DD-MM-YYYY");

const getPEData = async (indexCode = 16, rp = rpn) => {
  let jsonData = {};
  try {
    jsonData = JSON.parse(
      await rp(
        `https://api.bseindia.com/BseIndiaAPI/api/MarketCap/w?code=${indexCode}`
      )
    );
  } catch (e) {
    console.log(e);
  }
  return jsonData;
};

const getPEFromJSON = jsonData => {
  try {
    if (jsonData && jsonData[0] && jsonData[0]["PE"]) {
      return parseFloat(jsonData[0]["PE"]);
    }
  } catch (e) {
    throw new Error("PE not found in JSON");
  }
};

const getLatestPEDocument = async PEClass => {
  let lastestDocument;
  try {
    lastestDocument = await PEClass.findOne({}).sort({ date: "desc" });
  } catch (e) {
    console.log(e);
  }
  return lastestDocument;
};

const savePEDocument = async (PENumber, momentObject, PEClass) => {
  const date = momentObject.toDate();
  date.setHours(0, 0, 0, 0);
  const peObject = new PEClass({
    date,
    pe: PENumber
  });
  try {
    await peObject.save();
  } catch (e) {
    console.log(e);
    throw new Error("Cannot save PE document");
  }
};

const shoudlEmailBeSent = (oldPENumber, newPENumber) => {
  return parseInt(oldPENumber) !== parseInt(newPENumber);
};

const generateEmailBody = (arrayOfPeValues, dateString) => {
  return `
  <table style="border: 1px solid black;">
    <tr>
      <th style="font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; overflow: hidden; word-break: normal; padding: 10px 5px; border: 1px solid black" align="center" valign="top">P/E ratio</th>
      <th style="font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; overflow: hidden; word-break: normal; padding: 10px 5px; border: 1px solid black" align="center" valign="top">Valuation</th>
      <th style="font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; overflow: hidden; word-break: normal; padding: 10px 5px; border: 1px solid black" align="center" valign="top">Investment Decision</th>
    </tr>
    <tr>
      <td style="font-family: Arial, sans-serif; font-size: 14px; overflow: hidden; word-break: normal; padding: 10px 5px; border: 1px solid black" valign="top">${
        arrayOfPeValues[0]
      }</td>
      <td style="font-family: Arial, sans-serif; font-size: 14px; overflow: hidden; word-break: normal; padding: 10px 5px; border: 1px solid black" valign="top">Extremely Inexpensive</td>
      <td style="font-family: Arial, sans-serif; font-size: 14px; overflow: hidden; word-break: normal; padding: 10px 5px; border: 1px solid black" valign="top">Rare Event. Screaming Buy. Sell your home</td>
    </tr>
    <tr>
      <td style="font-family: Arial, sans-serif; font-size: 14px; overflow: hidden; word-break: normal; padding: 10px 5px; border: 1px solid black" valign="top">${
        arrayOfPeValues[1]
      }</td>
      <td style="font-family: Arial, sans-serif; font-size: 14px; overflow: hidden; word-break: normal; padding: 10px 5px; border: 1px solid black" valign="top">Inexpensive</td>
      <td style="font-family: Arial, sans-serif; font-size: 14px; overflow: hidden; word-break: normal; padding: 10px 5px; border: 1px solid black" valign="top">Screaming Buy</td>
    </tr>
    <tr>
      <td style="font-family: Arial, sans-serif; font-size: 14px; overflow: hidden; word-break: normal; padding: 10px 5px; border: 1px solid black" valign="top">${
        arrayOfPeValues[2]
      }</td>
      <td style="font-family: Arial, sans-serif; font-size: 14px; overflow: hidden; word-break: normal; padding: 10px 5px; border: 1px solid black" valign="top">Average</td>
      <td style="font-family: Arial, sans-serif; font-size: 14px; overflow: hidden; word-break: normal; padding: 10px 5px; border: 1px solid black" valign="top">Buy or hold</td>
    </tr>
    <tr>
      <td style="font-family: Arial, sans-serif; font-size: 14px; overflow: hidden; word-break: normal; padding: 10px 5px; border: 1px solid black" valign="top">${
        arrayOfPeValues[3]
      }</td>
      <td style="font-family: Arial, sans-serif; font-size: 14px; overflow: hidden; word-break: normal; padding: 10px 5px; border: 1px solid black" valign="top">Expensive</td>
      <td style="font-family: Arial, sans-serif; font-size: 14px; overflow: hidden; word-break: normal; padding: 10px 5px; border: 1px solid black" valign="top">Book 80% Profit. Wait for better entry levels</td>
    </tr>
    <tr>
      <td style="font-family: Arial, sans-serif; font-size: 14px; overflow: hidden; word-break: normal; padding: 10px 5px; border: 1px solid black" valign="top">${
        arrayOfPeValues[4]
      }</td>
      <td style="font-family: Arial, sans-serif; font-size: 14px; overflow: hidden; word-break: normal; padding: 10px 5px; border: 1px solid black" valign="top">Very Expensive</td>
      <td style="font-family: Arial, sans-serif; font-size: 14px; overflow: hidden; word-break: normal; padding: 10px 5px; border: 1px solid black" valign="top">Rare event. Screaming Sell.</td>
    </tr>
  </table>
  <p>Date: ${dateString}</p>
  <p>*THESE P/E NUMBERS CORRESPOND TO CONSOLIDATED PROFITS, NOT STANDALONE PROFITS!</p>
  `;
};

const sendEmail = async (
  oldPENumber,
  newPENumber,
  context,
  indexName,
  arrayOfPeValues,
  dateString
) => {
  oldPENumber = parseInt(oldPENumber);
  newPENumber = parseInt(newPENumber);

  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: context.secrets.EMAIL_USER,
      pass: context.secrets.EMAIL_PASS
    }
  });

  const mailOptions = {
    from: context.secrets.EMAIL_USER,
    bcc: context.secrets.EMAIL_TO.split(","),
    subject: `${indexName} P/E ratio has changed a major digit!`,
    html:
      `<p>${indexName} P/E ratio has changed <strong> from ${oldPENumber}.x to ${newPENumber}.x</strong></p>` +
      generateEmailBody(arrayOfPeValues, dateString)
  };
  try {
    await transporter.sendMail(mailOptions);
  } catch (e) {
    console.log(e);
  }
};

const main = async (context, cb) => {
  if (!context) {
    context = { secrets: process.env };
  }

  const result = [];
  const indices = context.secrets.INDICES.split(",");
  const bseIndexMap = {
    SENSEX_50: 98,
    SENSEX_NEXT_50: 99,
    MIDCAP_150: 102
  };
  try {
    await mongoose.connect(context.secrets["DB_URL"], {
      useNewUrlParser: true
    });
  } catch (e) {
    cb(e);
    return;
  }

  const yesterday = moment().subtract(1, "days");
  const yesterdayFormatted = formatMomentObject(yesterday);
  for (let index of indices) {
    try {
      const jsonData = await getPEData(bseIndexMap[index]);
      const pe = getPEFromJSON(jsonData);
      result.push(pe);

      const PE = mongoose.model(index, schema);
      const latestPEDocument = await getLatestPEDocument(PE); // Gives day before yesterday's PE
      if (!latestPEDocument || latestPEDocument.pe !== pe) {
        await savePEDocument(pe, yesterday, PE);
      }

      if (latestPEDocument && shoudlEmailBeSent(latestPEDocument.pe, pe)) {
        await sendEmail(
          latestPEDocument.pe,
          pe,
          context,
          context.secrets[`${index}_NAME`],
          context.secrets[`${index}_PE_VALUES`].split("|"),
          yesterdayFormatted
        );
      }
    } catch (e) {
      result.push(e.toString());
    }
  }

  try {
    await mongoose.disconnect();
  } catch (e) {
    console.log(e);
  }
  cb(null, result);
};

module.exports = main;
module.exports.getPEData = getPEData;
module.exports.formatMomentObject = formatMomentObject;
module.exports.getPEFromJSON = getPEFromJSON;
module.exports.getLatestPEDocument = getLatestPEDocument;
module.exports.shoudlEmailBeSent = shoudlEmailBeSent;
module.exports.sendEmail = sendEmail;
// main(null, (e, v) => console.log(e || v))
