module.exports = [
  {
    Full: '6,483,785.48',
    FreeFloat: '3,644,263.99',
    PE: '23.94',
    PB: '3.04',
    DivYield: '1.19',
    AdvScrp: '16',
    AdvTO: '406.23',
    DecScrp: '15',
    DecTO: '605.87',
    UnchgScrp: '0',
    UnchgTO: '0.00',
    NonTrdScrp: '0',
    NonTrdTO: '0.00',
    TotalScrp: '31',
    TotalTO: '1,012.10',
    Dt: '20 Dec 2018'
  }
]
