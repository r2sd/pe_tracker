/* global it */
const assert = require("assert");
const getPEFromJSON = require("../pe_tracker").getPEFromJSON;
const formatMomentObject = require("../pe_tracker").formatMomentObject;
const getLatestPEDocument = require("../pe_tracker").getLatestPEDocument;
const shoudlEmailBeSent = require("../pe_tracker").shoudlEmailBeSent;
const sendEmail = require("../pe_tracker").sendEmail;
const sensexResponse = require("./sensex_response");
const moment = require("moment-timezone");

require("dotenv").config();

it("test getPEFromJSON", () => {
  assert.strictEqual(true, typeof getPEFromJSON(sensexResponse) === "number");
});

it("test getLatestPEDocument when latest document is available", async () => {
  let latestPEDocument = await getLatestPEDocument({
    findOne: () => {
      return {
        sort: () =>
          Promise.resolve({
            date: new Date("2018-07-20T00:00:00.000Z"),
            pe: 27.36
          })
      };
    }
  });

  assert.strictEqual(
    formatMomentObject(moment(latestPEDocument.date)),
    formatMomentObject(moment("2018-07-20"))
  );
  assert.strictEqual(latestPEDocument.pe, 27.36);
});

it("test getLatestPEDocument when latest document is NOT available", async () => {
  let latestPEDocument = await getLatestPEDocument({
    findOne: () => {
      return {
        sort: () => Promise.resolve(null)
      };
    }
  });

  assert.strictEqual(latestPEDocument, null);
});

it("test shoudlEmailBeSent", () => {
  assert.strictEqual(shoudlEmailBeSent(1.9, 1.1), false);
  assert.strictEqual(shoudlEmailBeSent(27.0, 27.1), false);
  assert.strictEqual(shoudlEmailBeSent(26.9, 27.1), true);
});

it("test sendEmail over network", async () => {
  await sendEmail(
    10,
    10,
    {
      secrets: {
        EMAIL_USER: process.env.EMAIL_USER,
        EMAIL_PASS: process.env.EMAIL_PASS,
        EMAIL_TO: "r2sd.inbox@gmail.com"
      }
    },
    "Bla Bla",
    ["0", "1", "2", "3", "4"],
    "01-01-2018"
  );
}).timeout(5000);
